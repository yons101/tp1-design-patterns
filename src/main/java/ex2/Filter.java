package ex2;

public interface Filter {
    int[] filter(int[] data);
}
