package ex2;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        Framework framework = new Framework();
        System.out.println(Arrays.toString(framework.filter(new int[]{1, 2, 3})));
        framework.setFilter(new FilterAdapter());
        System.out.println(Arrays.toString(framework.filter(new int[]{1, 2, 3})));
        System.out.println(Arrays.toString(framework.compresser(new int[]{1, 2, 3})));
    }
}
