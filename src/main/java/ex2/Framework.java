package ex2;

public class Framework {
    private Filter filter = new FilterImpl();
    private Compresser compresser = new CompresserImpl();

    public int[] filter(int[] data) {
        return filter.filter(data);
    }

    public int[] compresser(int[] data) {
        return compresser.compresser(data);
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public void setCompresser(Compresser compresser) {
        this.compresser = compresser;
    }
}
